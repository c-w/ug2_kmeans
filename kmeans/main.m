% Pixel Clustering Script
% http://www.inf.ed.ac.uk/teaching/courses/inf2b/coursework/inf2b-cwk2-2012.pdf

tic;
clearvars *;
fun_dir = './utils';
lib_dir = './libs';
data_dir = './data';
plot_dir = './plots';
if not(exist(fun_dir, 'dir'));
    disp(sprintf('critical error: can''t find %s', fun_dir));
    return;
else;
    addpath(fun_dir);
end;
if not(exist(lib_dir, 'dir'));
    disp(sprintf('critical error: can''t find %s', lib_dir));
    return;
else;
    addpath(lib_dir);
end;
if not(exist(data_dir, 'dir'));
    disp(sprintf('critical error: can''t find %s', data_dir));
    return;
else;
    addpath(data_dir);
end;
if not(exist(plot_dir, 'dir'));
    mkdir(plot_dir);
end;
load data_90.mat;
load data_900.mat;
[data90_numVectors, ~] = size(data_90);

data90_distanceMatrix = NaN(data90_numVectors, data90_numVectors);
max_distance = 0;
data90_mean1 = data_90(1,:);
data90_mean2 = data_90(1,:);
for i = 1:data90_numVectors;
    for j = 1:data90_numVectors;
        if i == j;
            data90_distanceMatrix(i, j) = 0;
            continue;
        end;
        
        % Compute a distance matrix between all points
        data90_distanceMatrix(i, j) = euclidean_distance( ...
                                        data_90(j,:), data_90(i,:));
        
        % Take 2 points that are furthest apart and set them as mean1 and mean2
        if data90_distanceMatrix(i, j) > max_distance;
            data90_mean1 = data_90(i,:);
            data90_mean2 = data_90(j,:);
            max_distance = data90_distanceMatrix(i, j);
        end;
    end;
end;
clearvars i j max_distance;

% For every additional mean required take the distances from a point to
% all the means and multiply them. Do this for each point. Make the
% point with the maximum resulting value a new mean.
data90_mean3 = farthest_from_points(data_90, [data90_mean1; data90_mean2]);
data90_mean4 = farthest_from_points(data_90, [data90_mean1; data90_mean2; ...
                                              data90_mean3]);
data90_mean5 = farthest_from_points(data_90, [data90_mean1; data90_mean2; ...
                                              data90_mean3; data90_mean4]);
two_means = [data90_mean1; data90_mean2];
three_means = [data90_mean2; data90_mean3; data90_mean1]; % fits to true_90.mat
four_means = [data90_mean1; data90_mean2; data90_mean3; data90_mean4];
five_means = [data90_mean1; data90_mean2; data90_mean3; data90_mean4; ...
              data90_mean5];

% Cluster the data in data 90.mat into 2, 3, 4, 5 clusters using k-means
[data90_2means, data90_2means_error] = k_means(data_90, two_means);
[data90_3means, data90_3means_error] = k_means(data_90, three_means);
[data90_4means, data90_4means_error] = k_means(data_90, four_means);
[data90_5means, data90_5means_error] = k_means(data_90, five_means);
plot_clusters(data90_2means, 'data_90 2-means plot', plot_dir);
plot_clusters(data90_3means, 'data_90 3-means plot', plot_dir);
plot_clusters(data90_4means, 'data_90 4-means plot', plot_dir);
plot_clusters(data90_5means, 'data_90 5-means plot', plot_dir);
clearvars two_means four_means five_means;

% Provide an sum-squared error plot
plot_sum_squared_error([data90_2means_error; data90_3means_error; ...
                        data90_4means_error; data90_5means_error], ...
                        'data_90 sum-squared-error plot', plot_dir);
clearvars data90_2means_error data90_3means_error data90_4means_error ...
          data90_5means_error;

% Obtain the clusters for 3-means using data 90.mat
data90_3means_cluster1 = data90_3means{1};
data90_3means_cluster2 = data90_3means{2};
data90_3means_cluster3 = data90_3means{3};
[len_cluster1, ~] = size(data90_3means_cluster1);
[len_cluster2, ~] = size(data90_3means_cluster2);
[len_cluster3, ~] = size(data90_3means_cluster3);

% Fit 3 full covariance Gaussians to the clusters
data90_3means_cluster1_gaussian = struct( ...
    'mean', mean_vector(data90_3means_cluster1), ...
    'covariance', covariance_matrix(data90_3means_cluster1, false), ...
    'prior', len_cluster1/data90_numVectors);
data90_3means_cluster2_gaussian = struct( ...
    'mean', mean_vector(data90_3means_cluster2), ...
    'covariance', covariance_matrix(data90_3means_cluster2, false), ...
    'prior', len_cluster2/data90_numVectors);
data90_3means_cluster3_gaussian = struct( ...
    'mean', mean_vector(data90_3means_cluster3), ...
    'covariance', covariance_matrix(data90_3means_cluster3, false), ...
    'prior', len_cluster3/data90_numVectors);
clearvars len_cluster1 len_cluster2 len_cluster3;

% Classify the data 900.mat using the Gaussians
data900_gaussians = gaussian_classifier(data_900, [ ...
                                       data90_3means_cluster1_gaussian; ...
                                       data90_3means_cluster2_gaussian; ...
                                       data90_3means_cluster3_gaussian]);
plot_clusters(data900_gaussians, 'data_900 gaussian-classifier plot', plot_dir);

% Cluster the data 900.mat using 3-means
[data900_3means, ~] = k_means(data_900, three_means);
plot_clusters(data900_3means, 'data_900 3-means plot', plot_dir);
clearvars three_means;

% Using the true clusters, provide confusion matrices for results
load true_900.mat;
[data900_3means_confusion, cor_3means] = confusion(data900_3means, true_900, ...
                                                   data_900);
disp(sprintf('-------------------------------------------\n'));
disp('data_900: 3-means clustering confusion matrix');
disp(sprintf('\t\t      true class'));
disptable(data900_3means_confusion, '1|2|3', ...
          'predicted class 1|predicted class 2|predicted class 3');
disp(sprintf('percent correct: %.2f%%\n', cor_3means * 100));
[data900_gaussians_confusion, cor_gaus] = confusion(data900_gaussians, ...
                                                    true_900, data_900);
disp(sprintf('-------------------------------------------\n'));
disp('data_900: gaussian classification confusion matrix');
disp(sprintf('\t\t      true class'));
disptable(data900_gaussians_confusion, '1|2|3', ...
          'predicted class 1|predicted class 2|predicted class 3');
disp(sprintf('percent correct: %.2f%%\n', cor_gaus * 100));
disp(sprintf('-------------------------------------------\n'));
clearvars data_90 data90_numVectors data_900 true_900 cor_3means cor_gaus ...
          fun_dir lib_dir data_dir plot_dir;
toc;
