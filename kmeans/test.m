% Unit tests for (most of the) custom functions used in main.m


addpath('./utils');

passed_euclidean_0 = euclidean_distance([1], [2]) == 1;

data_1 =  [ 1, 1;
            3, 1;
           10, 0;
            1, 2;
            5, 2;
            4, 5;
            5, 4;
            6, 6;
            7, 6;
            8, 4;
           10, 5;
            7, 8;
            2, 9;
            4,13];
means_1 = [ 4,13;
           10, 0;
            1, 1];
expected_1_1 = [ 4,13;
                 2, 9;
                 7, 8];
expected_1_2 = [ 6, 6;
                 7, 6;
                 8, 4;
                10, 5;
                10, 0];
expected_1_3 = [ 1, 1;
                 3, 1;
                 1, 2;
                 5, 2;
                 5, 4;
                 4, 5];
[clusters_1, error_1] = k_means(data_1, means_1);
passed_clusters_1 = isequal(sortrows(clusters_1{1}), ...
                            sortrows(expected_1_1)) && ...
                    isequal(sortrows(clusters_1{2}), ...
                            sortrows(expected_1_2)) && ...
                    isequal(sortrows(clusters_1{3}), ...
                            sortrows(expected_1_3));


data_2 = [ 1, 1;
           4, 4;
           5, 1;
           7, 1;
           7, 4;
           7,10];
means_2 = [ 1, 1;
            7,10];
expected_2_1 = [ 1, 1;
                 4, 4;
                 5, 1;
                 7, 1];
expected_2_2 = [ 7, 4;
                 7,10];
[clusters_2, error_2] = k_means(data_2, means_2);
passed_clusters_2 = isequal(sortrows(clusters_2{1}), ...
                            sortrows(expected_2_1)) && ...
                    isequal(sortrows(clusters_2{2}), ...
                            sortrows(expected_2_2));


data_3 = [ 1, 1;
           5, 1;
           5, 3;
           9, 3];
means_3_a = [ 3, 1;
              7, 3];
expected_3_a_1 = [ 1, 1;
                   5, 1];
expected_3_a_2 = [ 5, 3;
                   9, 3];
expected_error_3_a = 4;
means_3_b = [ 1, 1;
              5, 3];
expected_3_b_1 = [ 1, 1];
expected_3_b_2 = [ 5, 1;
                   5, 3;
                   9, 3];
expected_error_3_b = 3.3333;
[clusters_3_a, error_3_a] = k_means(data_3, means_3_a);
[clusters_3_b, error_3_b] = k_means(data_3, means_3_b);
passed_clusters_3_a = isequal(sortrows(clusters_3_a{1}), ...
                              sortrows(expected_3_a_1)) && ...
                      isequal(sortrows(clusters_3_a{2}), ...
                              sortrows(expected_3_a_2));
passed_sqerror_3_a = isequal(num2str(expected_error_3_a), num2str(error_3_a));
passed_clusters_3_b = isequal(sortrows(clusters_3_b{1}), ...
                              sortrows(expected_3_b_1)) && ...
                      isequal(sortrows(clusters_3_b{2}), ...
                              sortrows(expected_3_b_2));
passed_sqerror_3_b = isequal(num2str(expected_error_3_b), num2str(error_3_b));


data_4 = [ 4.0, 2.0, .60;
           4.2, 2.1, .59;
           3.9, 2.0, .58;
           4.3, 2.1, .62;
           4.1, 2.2, .63];
expected_mean_4 = [4.10, 2.08, .604];
expected_covariance_4 = [ 0.025,   0.0075,  0.00175;
                          0.0075,  0.0070,  0.00135;
                          0.00175, 0.00135, 0.00043];
passed_meanvector_4 = isequal(num2str(expected_mean_4), ...
                              num2str(mean_vector(data_4)));
passed_covariance_4 = isequal(num2str(expected_covariance_4), ...
                              num2str(covariance_matrix(data_4, true)));


data_5_S = [10; 8; 10; 10; 11; 11];
data_5_T = [12; 9; 15; 10; 13; 13];
gaussian_S = struct( ...
                'mean', mean_vector(data_5_S), ...
                'covariance', covariance_matrix(data_5_S, true));
gaussian_T = struct( ...
                'mean', mean_vector(data_5_T), ...
                'covariance', covariance_matrix(data_5_T, true));
passed_assign_10_to_S = log_likelihood(10, gaussian_S) > ...
                        log_likelihood(10, gaussian_T);
passed_assign_11_to_S = log_likelihood(11, gaussian_S) > ...
                        log_likelihood(11, gaussian_T);
passed_assign_6_to_T = log_likelihood(6, gaussian_S) < ...
                       log_likelihood(6, gaussian_T);
passed_log_likelihood_5 = passed_assign_10_to_S && passed_assign_11_to_S && ...
                          passed_assign_6_to_T;
gaussian = gaussian_classifier([10; 11; 6], [gaussian_S; gaussian_T]);
passed_univariate_classifier_5 = isequal(gaussian{1}, [10; 11]) && ...
                                 isequal(gaussian{2}, [6]);


errors = 0;

if not(passed_euclidean_0);
    errors = errors + 1;
    disp('test 0: bad euclidean distance');
end;

if not(passed_clusters_1);
    errors = errors + 1;
    disp('test 1: some bad clusters');
end;

if not(passed_clusters_2);
    errors = errors + 1;
    disp('test 2: some bad clusters');
end;

if not(passed_clusters_3_a);
    errors = errors + 1;
    disp('test 3_a: some bad clusters');
end;

if not(passed_sqerror_3_a);
    errors = errors + 1;
    disp('test 3_a: bad sum-squared error');
end;

if not(passed_clusters_3_b);
    errors = errors + 1;
    disp('test 3_b: some bad clusters');
end;

if not(passed_sqerror_3_b);
    errors = errors + 1;
    disp('test 3_b: bad sum-squared error');
end;

if not(passed_meanvector_4);
    errors = errors + 1;
    disp('test 4: bad mean vector');
end;

if not(passed_covariance_4);
    errors = errors + 1;
    disp('test 4: bad covariance matrix');
end;

if not(passed_log_likelihood_5);
    errors = errors + 1;
    disp('test 5: bad log likelihood assignment');
end;

if not(passed_univariate_classifier_5);
    errors = errors + 1;
    disp('test 5: bad gaussian classification');
end;

if errors == 0;
    disp('passed all tests')
else;
    disp(sprintf('\n%d errors in total', errors));
end;
