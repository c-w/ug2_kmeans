% Returns a new vector that is the mean of the elements of $data

function meanv = mean_vector(data)

[num_data, point_len] = size(data);
meanv_dummy = zeros(1, point_len);

for i = 1:point_len;
    meanv_dummy(i) = sum(data(1:num_data, i)) / num_data;
end;

meanv = meanv_dummy;
