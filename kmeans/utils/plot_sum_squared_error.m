function sum_squared_error_plot(errors, title_str, path)

[len, ~] = size(errors);
errors = [NaN; errors; NaN];

x = 2:1:(len+1);
y = errors(x);
figure('visible', 'off');
plot(x, y, 'r:', 'Marker', '*', 'MarkerSize', 8, 'MarkerEdgeColor', 'black');
set(gca,'XTick', x);
title(strrep(title_str, '_', '\_'));
xlabel('Number of Clusters');
ylabel('Sum Squared Error');
axis([1.5, (len+1.5), min(errors)*0.85, max(errors)*1.05])
saveas(gca, sprintf('%s/%s.png', path, regexprep(title_str, ...
                                                 '[^a-zA-Z0-9]', '_')));
close();
