% Returns the element of $data for which the product of distances to all 
% elements of $points is maximal.

function faraway = farthest_from_points(data, points)

[num_vectors, vector_len] = size(data);
[num_points, point_len] = size(points);
distance_matrix = zeros(num_vectors, num_points);
max_distance = -Inf;
faraway_dummy = data(1,:);

for i = 1:num_vectors;
    for j = 1:num_points;
        distance_matrix(i, j) = euclidean_distance(points(j,:), data(i,:));
    end;
    
    mult = prod(distance_matrix(i,:));
    if mult > max_distance;
        faraway_dummy = data(i,:);
        max_distance = mult;
    end;
end;

faraway = faraway_dummy;
