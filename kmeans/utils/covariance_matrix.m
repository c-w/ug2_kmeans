% Returns the covariance matrix for $data.
% If $estimated is set, the population covariance is returned, otherwise the 
% estimated covariance is returned.

function covarm = covariance_matrix(data, estimated)

[num_data, data_len] = size(data);
covarm_dummy = NaN(num_data, data_len);
means = mean_vector(data);
correction = 0;
if estimated;
    correction = 1;
end;

% subtract column-mean from elements
for i = 1:num_data;
    for j = 1:data_len;
        covarm_dummy(i, j) = data(i, j) - means(j);
    end;
end;

% reshape matrix
covarm_dummy = transpose(covarm_dummy) * covarm_dummy / (num_data - correction);

covarm = covarm_dummy;
