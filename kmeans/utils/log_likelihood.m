% Returns the log-likelihood of some vector w.r.t $gaussian

function ll = log_likelihood(x, gaussian)

[~, d] = size(x);
m = gaussian.mean;
c = gaussian.covariance;
try;
    p = gaussian.prior;
catch nonExistentField;
    p = 1;
end;

ll = -d/2*log(2*pi) - log(abs(c))/2 - 1/2*(x-m)*inv(c)*transpose(x-m) + log(p);
