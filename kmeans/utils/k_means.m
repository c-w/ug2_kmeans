% Performs k-means clustering on $data using $means as initial mean-points

function [clusters, sqerror] = k_means(data, means)

[num_data, ~] = size(data);
[num_means, ~] = size(means);
clusters_dummy = cell(num_means, 1);

converged = 0;
while not(converged);
    old_clusters = clusters_dummy;
    
    % update clusters
    clusters_dummy = assign_to_nearest(data, means);
    
    % update means
    for i = 1:num_means;
        means(i,:) = mean_vector(clusters_dummy{i});
    end;
    
    % exit loop if clusters have converged
    for i = 1:num_means;
        converged = isequal(clusters_dummy{i}, old_clusters{i});
        if converged == 0;
            break;
        end;
    end;
end;

% calculate sum-squared-error = distance from points to cluster mean
sqerror_dummy = 0;
for i = 1:num_means;
    cluster = clusters_dummy{i};
    [num_points_cluster, ~] = size(cluster);
    
    for j = 1:num_points_cluster;
        sqerror_dummy = sqerror_dummy + ...
                        euclidean_distance(cluster(j,:), means(i,:))^2;
    end;
end;
sqerror_dummy = sqerror_dummy / num_data;

clusters = clusters_dummy;
sqerror = sqerror_dummy;
