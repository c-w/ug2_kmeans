% Assigns the points in $data to the closest point in $means

function clusters = assign_to_nearest(data, means)

[num_data, data_len] = size(data);
[num_means, mean_len] = size(means);
clusters_dummy = cell(num_means, 1);

for i = 1:num_data;
    closest_index = 1;
    closest_distance = euclidean_distance(means(1,:), data(i,:));
    
    % find closest point in $means
    for j = 2:num_means;
        distance_to_mean = euclidean_distance(means(j,:), data(i,:));
        if distance_to_mean < closest_distance;
            closest_index = j;
            closest_distance = distance_to_mean;
        end;
    end;
    
    % assign to cluster corresponding to point previously found
    clusters_dummy{closest_index} = [clusters_dummy{closest_index}; data(i,:)];
end;

clusters = clusters_dummy;
