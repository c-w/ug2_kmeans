% Assigns points in $data to gaussian in $gaussians such that the log-likelihood
% is maximised.

function clusters = gaussian_classifier(data, gaussians)

[num_data, ~] = size(data);
[num_gaussians, ~] = size(gaussians);
clusters_dummy = cell(num_gaussians, 1);

for i = 1:num_data;
    max_ll = -Inf;
    ind = 0;
    
    % calculate log-likelihood of point for each gaussian
    for j = 1:num_gaussians;
        ll = log_likelihood(data(i,:), gaussians(j));
        if ll > max_ll;
            max_ll = ll;
            ind = j;
        end;
    end;
    
    % assign point to gaussian with highest log-likelihood
    clusters_dummy{ind} = [clusters_dummy{ind}; data(i,:)];
end;

clusters = clusters_dummy;
