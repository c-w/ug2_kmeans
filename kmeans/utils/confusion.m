% Returns the confusion matrix of $predicted_values vs. $true_values of $data
% and the rate of correctly classified points

function [confm, good] = confusion_matrix(predicted_values, true_values, data);

[num_vectors, vector_len] = size(data);
[num_clusters, ~] = size(predicted_values);

confm_dummy = zeros(vector_len, vector_len);
for i = 1:num_vectors;
    for j = 1:num_clusters;
        % find point in $predicted_values
        if strmatch(data(i,:), predicted_values{j});
            confm_dummy(j, true_values(i)) = confm_dummy(j, true_values(i)) + 1;
        end;
    end;
end;

confm = confm_dummy;
good = trace(confm_dummy) / num_vectors;
