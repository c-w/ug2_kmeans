% Returns the Euclidean distance between $vector1 and $vector2

function distance = euclidean_distance(vector1, vector2)

distance = sqrt(sum((vector1-vector2).^2));
