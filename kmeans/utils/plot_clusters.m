function plot_clusters(clusters, title_str, path)

colors = [[0 0 0]; [1 0 1]; [0 1 1]; [1 0 0]; [0 1 0]; [0 0 1]; [1 1 0];];
[num_clusters, ~] = size(clusters);
xs = [];
ys = [];
zs = [];
cs = [];

for i = 1:num_clusters;
    cluster = clusters{i};
    [num_points, ~] = size(cluster);
    xs = [xs; cluster(1:num_points, 1)];
    ys = [ys; cluster(1:num_points, 2)];
    zs = [zs; cluster(1:num_points, 3)];
    cs = [cs; repmat(colors(i,:), num_points, 1)];
end;

figure('visible', 'off');
scatter3(xs, ys, zs, 25, cs, 'Marker', '*');
title(strrep(title_str, '_', '\_'));
saveas(gca, sprintf('%s/%s.png', path, regexprep(title_str, ...
                                                 '[^a-zA-Z0-9]', '_')));
close();
